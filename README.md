Trustpilot Technical Test
-------------------------

This is my submission for the Trustpilot technical tests.
This includes solutions for the anagrams and maze puzzles.
There is commentary and exploration in the notebooks, and a direct reasonably concise solution in the `summary.py` files.

### Running the code

This assumes you have [pipenv](https://pipenv.readthedocs.io/en/latest/) installed and configured, alongside [pyenv](https://github.com/pyenv/pyenv).
The solutions are written in python, as that was one of the current technologies in use.

You can install the dependencies and create the virtualenv as normal:

```bash
pipenv install
```

You can then invoke the summary scripts either directly or via python:

```bash
pipenv run python anagrams/summary.py
```

```bash
pipenv run python mazes/summary.py
```

To review the notebooks use `jupyter-lab`:

```bash
pipenv run jupyter-lab
```
This should open the lab in your browser.

### Example Output

```
➜ pipenv run python anagrams/summary.py
16898346it [00:24, 680567.91it/s]
16,907,715: 'wu lisp not statutory' hashes to 665e5bcb0c20062fe8abaaf4628bb154
144483736it [03:28, 669990.69it/s]
144,520,415: 'ty outlaws printouts' hashes to 23170acc097c24edb98fc5488ab033fe
573280780it [13:41, 680098.32it/s]
573,287,773: 'printout stout yawls' hashes to e4820b45d2277f3844eac66c903e84be
573280780it [13:41, 697614.65it/s]
```

```
➜ pipenv run python mazes/summary.py --width 15 --height 15 --difficulty 0
Started maze: b2c0a11a-cc89-47d3-9f02-2cd6f691b2f7
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked west: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked west: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked west: {'state': 'active', 'state-result': 'Move accepted'}
Walked west: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked west: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked west: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked north: {'state': 'active', 'state-result': 'Move accepted'}
Walked east: {'state': 'active', 'state-result': 'Move accepted'}
Walked south: {'state': 'active', 'state-result': 'Move accepted'}
Walked south: {'state': 'active', 'state-result': 'Move accepted'}
Walked south: {'state': 'active', 'state-result': 'Move accepted'}
Walked south: {'state': 'won', 'state-result': 'You won. Game ended', 'hidden-url': '/eW91X3NhdmVkX3RoZV9wb255.jpg'}
```

### Deep Learning Mazes

I did have a go at this after all.
Just the simple CNN approach.
There is a notebook if you wish to review it, `maze-dl.ipynb` in the mazes folder.

The approach did not work out.
There are more things I could've tried.
