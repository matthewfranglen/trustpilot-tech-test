#!/usr/bin/env python3

from __future__ import annotations
from collections import Counter
from dataclasses import dataclass
from itertools import permutations
from pathlib import Path
from typing import Dict, Iterator, List, Union
import hashlib

from tqdm import tqdm

WORDS_FILE = Path(__file__).parent / "wordlist"
ANAGRAM = "poultry outwits ants"
HASHES = [
    "e4820b45d2277f3844eac66c903e84be",
    "23170acc097c24edb98fc5488ab033fe",
    "665e5bcb0c20062fe8abaaf4628bb154",
]


def main() -> None:
    solutions = []

    for idx, anagram in tqdm(enumerate(search_count(ANAGRAM, WORDS_FILE))):
        anagram_hash = md5(anagram)
        if anagram_hash in HASHES:
            solutions.append((anagram, anagram_hash, idx))
            print(f"\n{idx:,}: '{anagram}' hashes to {anagram_hash}")

            if len(solutions) >= 3:
                break


def md5(text: str) -> str:
    md5_builder = hashlib.md5()
    md5_builder.update(bytes(text, "utf-8"))
    return md5_builder.hexdigest()


@dataclass(frozen=True)
class LetterCount:
    letters: Dict[str, int]

    @staticmethod
    def from_word(word: str) -> LetterCount:
        count = dict(Counter(word))
        count.pop(" ", None)  # remove counts for space
        return LetterCount(letters=count)

    def __sub__(self, other: Union[LetterCount, Word]) -> LetterCount:
        updated = self.letters.copy()
        for key, value in other.letters.items():
            updated[key] -= value
            if updated[key] < 0:
                raise ValueError("Not enough letters")
            if not updated[key]:
                del updated[key]
        return LetterCount(letters=updated)

    def __bool__(self) -> bool:
        return bool(self.letters)

    def __contains__(self, other: Union[LetterCount, Word]) -> bool:
        return all(
            self.letters.get(letter, 0) >= count
            for letter, count in other.letters.items()
        )


@dataclass(frozen=True)
class Word:
    word: str
    count: LetterCount

    @staticmethod
    def from_word(word: str) -> Word:
        return Word(word=word, count=LetterCount.from_word(word))

    @property
    def letters(self) -> Dict[str, int]:
        return self.count.letters

    def __len__(self) -> int:
        return len(self.word)


def read_words(path: Path) -> List[Word]:
    return [Word.from_word(word) for word in set(path.read_text().splitlines())]


def reduce(anagram: LetterCount, words: List[Word]) -> List[Word]:
    return sorted([word for word in words if word in anagram], key=len, reverse=True,)


def search_count(anagram: str, wordlist: Path) -> Iterator[str]:
    letters = LetterCount.from_word(anagram)
    words = reduce(letters, read_words(wordlist))
    for words in _search_count(letters, words):
        for order in permutations(words):
            yield " ".join(order)


def _search_count(remainder: LetterCount, words: List[Word]) -> Iterator[List[str]]:
    if not words:
        return None
    assert remainder
    for idx, word in enumerate(words):
        word_remainder = remainder - word
        if not word_remainder:
            yield [word.word]
        else:
            for suffix in _search_count(
                word_remainder, reduce(word_remainder, words[idx + 1 :])
            ):
                yield [word.word] + suffix
    return None


if __name__ == "__main__":
    main()
