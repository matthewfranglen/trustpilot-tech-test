#!/usr/bin/env python3

from __future__ import annotations
from pathlib import Path
from typing import Any, Dict, List
import argparse
import json

import requests
import networkx as nx

DATA_FOLDER = Path(__file__).parent / "data"


def main():
    parser = argparse.ArgumentParser(description="Help a Pony through a Maze")
    parser.add_argument(
        "--width",
        type=int,
        choices=range(15, 26),
        required=True,
        help="width of maze, between 15 and 25 inclusive",
    )
    parser.add_argument(
        "--height",
        type=int,
        choices=range(15, 26),
        required=True,
        help="height of maze, between 15 and 25 inclusive",
    )
    parser.add_argument(
        "--difficulty",
        type=int,
        choices=range(0, 11),
        required=True,
        help="difficulty of maze enemy, between 0 and 10 inclusive",
    )
    args = parser.parse_args()

    run_sim(args.width, args.height, args.difficulty)


def run_sim(width: int, height: int, difficulty: int):
    maze_id = request_maze(width, height, difficulty)
    print(f"Started maze: {maze_id}")

    maze_data = request_maze_data(maze_id)
    record_maze_data(maze_data, 0, DATA_FOLDER)
    request_and_record_maze_rendition(maze_data, 0, DATA_FOLDER)
    path = calculate_path(maze_data)

    for turn, direction in enumerate(path, start=1):
        response = request_maze_step(maze_id, direction)
        print(f"Walked {direction}: {response}")
        if response["state"] != "active":
            break
        maze_data = request_maze_data(maze_id)
        record_maze_data(maze_data, turn, DATA_FOLDER)


def calculate_path(maze: Dict[str, Any]) -> List[str]:
    pony = maze["pony"][0]
    end = maze["end-point"][0]
    width, _ = maze["size"]

    G = maze_to_graph(maze)
    idx_steps = nx.astar_path(G, pony, end)
    return cardinal_steps(idx_steps, width)


def maze_to_graph(maze: Dict[str, Any]) -> nx.Graph:
    G = nx.Graph()
    G.add_nodes_from(range(len(maze["data"])))
    width, _ = maze["size"]
    G.add_edges_from(
        [
            (idx, idx - width)
            for idx, node in enumerate(maze["data"])
            if "north" not in node
        ]
    )
    G.add_edges_from(
        [(idx, idx - 1) for idx, node in enumerate(maze["data"]) if "west" not in node]
    )
    return G


def cardinal_steps(steps: List[int], width: int) -> List[str]:
    i_steps = iter(steps)
    next(i_steps)
    return [to_direction(start, end, width) for start, end in zip(steps, i_steps)]


def to_direction(start: int, end: int, width: int) -> str:
    assert start != end
    if start > end:  # left or up
        if start - end == 1:  # left
            return "west"
        assert start - end == width
        return "north"
    if end - start == 1:
        return "east"
    assert end - start == width
    return "south"


def request_maze(width: int, height: int, difficulty: int) -> str:
    assert 15 <= width <= 25
    assert 15 <= height <= 25
    assert 0 <= difficulty <= 10

    data = {
        "maze-width": width,
        "maze-height": height,
        "maze-player-name": "Rainbow Dash",
        "difficulty": difficulty,
    }

    response = requests.post(
        "https://ponychallenge.trustpilot.com/pony-challenge/maze", json=data
    )
    return response.json()["maze_id"]


def request_maze_data(maze_id: str) -> Dict[str, Any]:
    assert maze_id
    response = requests.get(
        f"https://ponychallenge.trustpilot.com/pony-challenge/maze/{maze_id}"
    )
    return response.json()


def record_maze_data(maze: Dict[str, Any], turn: int, folder: Path) -> None:
    maze_id = maze["maze_id"]
    width, height = maze["size"]
    difficulty = maze["difficulty"]

    maze_folder = folder / f"w{width}-h{height}-d{difficulty}" / maze_id
    if not maze_folder.exists():
        maze_folder.mkdir(parents=True)
    maze_file = maze_folder / f"{turn:04}.json"
    maze_file.write_text(json.dumps(maze))


def request_and_record_maze_rendition(
    maze: Dict[str, Any], turn: int, folder: Path
) -> Dict[str, Any]:
    maze_id = maze["maze_id"]
    width, height = maze["size"]
    difficulty = maze["difficulty"]

    response = requests.get(
        f"https://ponychallenge.trustpilot.com/pony-challenge/maze/{maze_id}/print"
    )

    maze_folder = folder / f"w{width}-h{height}-d{difficulty}" / maze_id
    if not maze_folder.exists():
        maze_folder.mkdir(parents=True)
    maze_file = maze_folder / f"{turn:04}-print.txt"
    maze_file.write_text(response.text)


def request_maze_step(maze_id: str, direction: str) -> None:
    # walking into wall (does advance enemy): { "state": "active", "state-result": "Can't walk in there" }
    # nonsense direction (NOT JSON!): Possible directions east, west, north, south, stay
    # good direction: { "state": "active", "state-result": "Move accepted" }
    # gruesome end: { "state": "over", "state-result": "You lost. Killed by monster", "hidden-url": "/eW91X2tpbGxlZF90aGVfcG9ueQ==.jpg" }
    # https://ponychallenge.trustpilot.com/eW91X2tpbGxlZF90aGVfcG9ueQ==.jpg it's beautiful! (it uses my pony too?)
    data = {"direction": direction}
    response = requests.post(
        f"https://ponychallenge.trustpilot.com/pony-challenge/maze/{maze_id}", json=data
    )
    return response.json()  # the end is unusual


if __name__ == "__main__":
    main()
